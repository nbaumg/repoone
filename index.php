<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Scientific Games problem</title>
    <style>
 		.outer {
	    width: 960px;
	    color: navy;
	    background-color: pink;
	    border: 2px solid darkblue;
	    padding: 5px;
		}
		</style>
  </head>
  <body>

  <h1>Problem 1</h1>
  															
	 	<?php

	 	class Persons{
	 		public $ID;
	 		public $BirthDate;
	 		public $EndDate;
	 	}

	 	$ArrPerson = array();
	 	for($x = 0; $x < 1000; $x++){
	 		$Person = new Persons();
	 		$Person->ID = rand(100000, 999999);
	 		$Person->BirthDate = rand(1900, 2000);
	 		$Person->EndDate = rand($Person->BirthDate,2000);
	 		$ArrPerson[$x] = $Person; 
	 	}

	 	echo "This is my Dataset (Randomly Generated)";

	 	$ArrFrequency = array_fill(1900, 2000, 0);
	 	for($x = 1900; $x < 2000; $x++){
	 		for($y = 0; $y < 1000; $y++){
	 			if($x > $ArrPerson[$y]->BirthDate && $x < $ArrPerson[$y]->EndDate){
	 				$ArrFrequency[$x]++;
	 			}
	 		}
	 	}?>

	 	<br>
	 	<br>
	 	<table>
	 		<td>
			 	<table border="1" cellpadding"5" cellspacing"0">
			 		<tr>
			 			<td>ID</td> <td>Birth Date</td> <td>End Date</td>
			 		</tr>
			 		<?php for($x = 0; $x < 250; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $ArrPerson[$x]->ID."\n";?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->BirthDate."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->EndDate."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
				</table>
			</td>
			<td>
				<table border="1" cellpadding"5" cellspacing"0">
					<tr>
				 			<td>ID</td> <td>Birth Date</td><td>End Date</td>
				 		</tr>
				 	<?php for($x = 250; $x < 500; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $ArrPerson[$x]->ID."\n";?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->BirthDate."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->EndDate."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
			 	</table>
			</td>
			<td>
				<table border="1" cellpadding"5" cellspacing"0">
					<tr>
						<td>ID</td> <td>Birth Date</td><td>End Date</td>
					</tr>
				 	<?php for($x = 500; $x < 750; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $ArrPerson[$x]->ID."\n";?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->BirthDate."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->EndDate."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
			 	</table>
		 	</td>
		 	<td>
				<table border="1" cellpadding"5" cellspacing"0">
					<tr>
						<td>ID</td> <td>Birth Date</td><td>End Date</td>
					</tr>
				 	<?php for($x = 750; $x < 1000; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $ArrPerson[$x]->ID."\n";?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->BirthDate."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrPerson[$x]->EndDate."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
			 	</table>
		 	</td>
	 	</table>
	 	<br>

	 	<?php	echo'These are the frequencies ';?>

	 	<br>
	 	<br>
	 	<table>
	 		<td>
			 	<table border="1" cellpadding"5" cellspacing"0">
			 		<tr>
						<td>Date</td> <td>Frequency</td>
					</tr>
			 		<?php for($x = 1900; $x < 1925; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $x."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrFrequency[$x]."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
				</table>
			</td>
			<td>
				<table border="1" cellpadding"5" cellspacing"0">
					<tr>
						<td>Date</td> <td>Frequency</td>
					</tr>
				 	<?php for($x = 1925; $x < 1950; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $x."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrFrequency[$x]."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
			 	</table>
			</td>
			<td>
				<table border="1" cellpadding"5" cellspacing"0">
					<tr>
						<td>Date</td> <td>Frequency</td>
					</tr>
				 	<?php for($x = 1950; $x < 1975; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $x."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrFrequency[$x]."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
			 	</table>
		 	</td>
		 	<td>
				<table border="1" cellpadding"5" cellspacing"0">
					<tr>
						<td>Date</td> <td>Frequency</td>
					</tr>
				 	<?php for($x = 1975; $x < 2000; $x++){ ?>
				 		<tr>
				 			<td>
				 			<?php	echo $x."\n"; ?>
						 	</td>
						 	<td>
				 			<?php	echo $ArrFrequency[$x]."\n"; ?>
						 	</td>
				 		</tr>
				 	<?php } ?>
			 	</table>
		 	</td>
	 	</table>
	 	<br>

	 	<?php
	 	$YearMostAlive = max($ArrFrequency);
	 	for($x = 1900; $x < 2000; $x++){
	 		if($YearMostAlive == $ArrFrequency[$x]){
	 			echo 'Year Most Alive = '.$x;
	 		}
	 	}
	 	?>
  </body>
</html>